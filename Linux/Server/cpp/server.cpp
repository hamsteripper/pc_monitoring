#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <thread>
#include <iostream>
#include <vector>
#include "nlohmann/json.hpp"
// #include <sensors-c++/sensors.h>


#define  OS_TYPE  0xF    //0xF0 - Windows, 0x0F - Linux
#if   OS_TYPE & 0xF0   // Windows
#include <Windows.h>
int  get_computer_name(char*  name, unsigned long size) {
     return GetComputerName(name, &size);
}
#else if OS_TYPE & 0x0F   // Linux
#include <cstdio>
#include <unistd.h>
#include <sys/utsname.h>
#include <cassert>
#include <vector>
#include <fstream>
#include <pwd.h>
#include <sys/sysinfo.h>

// MAXHOSTNAMELEN
// int  get_computer_name(char*  name, unsigned long size) {
//      return gethostname(name, size);
// }
#endif

#define PORT 5000
#define QUEUE 20
using namespace std;
using json = nlohmann::json;
int conn;
static int bufferSite = 1024;

struct DATA
{
    std::string hostname;
    std::string username;
    std::string uptime;
    std::string processor;
    std::string frequency;
    std::string cpuLoad;
    std::string totalMemory;
    std::string useMemory;
    std::string memoryPercent;
};

// Получение подстроки из файлов в директории proc
string getSubString(string line, string findParameter){
  string str;
  std::string::size_type pos = line.find(findParameter);
  if(pos!=string::npos){
    std::string::size_type pos = line.find(":");
    if(pos!=string::npos){
      str = line.substr(pos+2);
    }
    return str;
  }
  return "";
}

// Чтение параметров цпу
vector<float> readCpuStats()
{
    vector<float> ret;
    ifstream stat_file("/proc/stat");
    if (!stat_file.is_open())
    {
        cout << "Unable to open /proc/stat" << std::endl;
        return ret;
    }
    int val;
    string tmp;
    stat_file >> tmp;
    for (int i = 0; i < 4; ++i)
    {
        stat_file >> val;
        ret.push_back(val);
    }
    stat_file.close();
    return ret;
}
// Загрузка цпу
int getCpuLoad(double dt)
{
    vector<float> stats1 = readCpuStats();
    sleep(dt);
    vector<float> stats2 = readCpuStats();
    int size1 = stats1.size();
    int size2 = stats2.size();
    if (!size1 || !size2 || size1 != size2) return 2;
    for (int i = 0; i < size1; ++i){
        stats2[i] = stats2[i] - stats1[i];
    }
    int sum = 1;
    for (int i = 0; i < size1; ++i){
        sum += stats2[i];
    }
    int load = 100 - (stats2[size2 - 1] * 100 / sum);
    return load;
}


json readStatistics(){

  string hostname;
  string username;
  string uptime;
  string processor;
  string frequency;
  string cpuLoad;
  string totalMemory;
  string freeMemory;
  string memoryPercent;
  string cpuTemp;

  // Получение имени операционной системы
  ifstream fin;
  fin.open("/proc/sys/kernel/hostname");
  getline(fin,hostname);
  uid_t uid = geteuid();
  passwd *pw = getpwuid(uid);
  username = pw->pw_name;
  struct sysinfo o;
  sysinfo(&o);
  long up = o.uptime;
  int hour = up/60/60;
  int min = (up - hour*60*60) / 60;
  int sec =  ((up - hour*60*60) - min*60);
  uptime = std::to_string(hour) + ":" + std::to_string(min) + ":" + std::to_string(sec);
  fin.close();

  // Получение информации о процессоре
  fin.open("/proc/cpuinfo");
  string cpuinfoline;
  int pos;
  while (std::getline(fin, cpuinfoline)) {
    if(processor == "")
      if((processor = getSubString(cpuinfoline, "model name")) != ""){/*fprintf(stderr, "%s\n", ("Processor: " + processor).c_str());*/}
    if(frequency == "")
      if((frequency = getSubString(cpuinfoline, "cpu MHz")) != ""){/*fprintf(stderr, "%s\n", ("Frequency: " + frequency).c_str());*/}
  }
  fin.close();

  // Получение информации о загрузке памяти
  fin.open("/proc/meminfo");
  string meminfoline;
  fin >> meminfoline;
  fin >> meminfoline;
  int num = atoi(meminfoline.c_str());
  int percent = num / 100;
  int gb = (num / 1024) / 1024;
  int mb = (num-gb*1024*1024) /1024;
  int kb = (num - (gb*1024*1024+mb*1024));
  if (gb > 0)
     totalMemory = std::to_string(gb) + " Gb ";
  else
     totalMemory = "";
  if (mb > 0)
     totalMemory += std::to_string(mb) + " Mb ";
  if (kb > 0)
     totalMemory += std::to_string(kb) + " Kb ";

  string statline;
  int free = 0;
  fin >> statline; fin >> statline; fin >> statline;
  fin >> statline; fin >> statline; fin >> statline;
  free += atoi(statline.c_str());
  num -= free;
  gb = free / 1024 / 1024;
  mb = (free - gb*1024*1024) / 1024;
  kb = (free - ((mb*1024) + (gb * 1024 * 1024)));
  if (gb > 0)
     freeMemory = std::to_string(gb) + " Gb ";
  else
     freeMemory = "";
  if (mb > 0)
     freeMemory += std::to_string(mb) + " Mb ";
  if (kb > 0)
     freeMemory += std::to_string(kb) + " Kb ";
  // Процент загрузки памяти
  memoryPercent = std::to_string(num / percent);
  fin.close();
  // Загрузка процессора
  cpuLoad = std::to_string(getCpuLoad(1));
  // Температура процессора
  fin.open("/sys/class/hwmon/hwmon0/temp1_input");
  fin >> cpuTemp;
  fin.close();
  double temp = atof(cpuTemp.c_str());
  temp = temp / 1000;
  double truncated = std::trunc(temp * 10) / 10;
  double rounded = std::floor(temp * 10 + 0.5) / 10;
  cpuTemp = to_string(rounded);
//   fprintf(stderr,cpuTemp.c_str());
//   fprintf(stderr,"\n");


//    sensors::subfeature temp {"/sys/class/hwmon/hwmon0/temp1_input"};
//   sensors::load_config();
//   double t = temp.read();
//   string s = to_string(t);
//   fprintf(stderr,s.c_str());
//   fprintf(stderr,"\n");

  json data;
  data["hostname"]  = hostname;
  data["username"]  = username;
  data["uptime"]    = uptime;
  data["processor"] = processor;
  data["frequency"] = frequency;
  data["cpuLoad"]   = cpuLoad;
  data["totalMemory"]   = totalMemory;
  data["freeMemory"]   = freeMemory;
  data["memoryPercent"]   = memoryPercent;
  data["cpuTemp"]   = cpuTemp;
  return data;
}


int main() {

    char buffer[bufferSite];
    std::string str = "";
    int count = true;
/*****************************************Socket create***********************************/
    int ss = socket(AF_INET, SOCK_STREAM, 0);
    socket.settimeout()
    struct sockaddr_in server_sockaddr;
    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_port = htons(PORT);
    server_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(ss, (struct sockaddr* ) &server_sockaddr, sizeof(server_sockaddr))==-1) {
        perror("bind");
        exit(1);
    }
    if(listen(ss, QUEUE) == -1) {
        perror("listen");
        exit(1);
    }
    struct sockaddr_in client_addr;
    socklen_t socket_length = sizeof(client_addr);
/*****************************************************************************************/
    fprintf(stderr,"Start\n");
    fflush(stdout);
    while(1) {
        conn = accept(ss, (struct sockaddr*)&client_addr, &socket_length);
        string str_conn = "conn = " + std::to_string(conn) + "\n";
        fprintf(stderr, str_conn.c_str());
        fflush(stdout);
        if( conn > 0 ) {
          str = "Connection";
          memset(buffer, 0 , bufferSite);
          strcpy(buffer, str.c_str());
          fprintf(stderr,"send_1\n");
          fflush(stdout);
          send(conn, buffer, str.length(), 0);
          fprintf(stderr,"send_2\n");
          fflush(stdout);
          while(1){
              memset(buffer, 0 , bufferSite);
              fprintf(stderr, "recv_len_1\n");
              int len = recv(conn, buffer, bufferSite, 0);
              string str_recv = "recv_len_2 = " + std::to_string(len) + "\n";
              fprintf(stderr, str_recv.c_str());
              fflush(stdout);
              if(len == 0 && count){
                sleep(5);
                count = false;
              }if(len != 0 && !count){
                count = true;
              }if(len == 0 && !count){
                count = true;
                break;
              }
              if(strcmp(buffer, "EXIT") == 0)
                break;
              else if(strcmp(buffer, "GET_DATA") == 0){
                json data = readStatistics();
                str = data.dump();
                memset(buffer, 0 , bufferSite);
                strcpy(buffer, str.c_str());
                string str_conn = "conn_send_3 = " + std::to_string(conn) + "\n";
                fprintf(stderr, str_conn.c_str());
                fflush(stdout);
                send(conn, buffer, str.length() , 0);
                str_conn = "conn_send_4 = " + std::to_string(conn) + "\n";
                fprintf(stderr, str_conn.c_str());
                fflush(stdout);
              }
            }
        }
        fprintf(stderr,"Close current connection\n");
        close(conn);
        sleep(1);
    }
    close(ss);
    fprintf(stderr,"Finish\n");
    fflush(stdout);
    return 0;
}

// asfasf
