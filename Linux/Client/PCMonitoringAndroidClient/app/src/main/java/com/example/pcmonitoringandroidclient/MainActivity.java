package com.example.pcmonitoringandroidclient;

//import android.support.v7.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.os.SystemClock.sleep;

/************************chart****************************/
//import java.text.DecimalFormat;
/*********************************************************/


//import static java.lang.Thread.sleep;


public class MainActivity extends AppCompatActivity
{
    private Button      mBtnOpen  = null;
    private Button      mBtnSend  = null;
    private Button      mBtnClose = null;
//    private EditText    mEdit     = null;
    private EditText    mHost     = null;
    private EditText    mPort     = null;
    private TextView    mViewer   = null;
    private Connection  mConnect  = null;


    private  String     HOST      = "";
    private  int        PORT      = 0;

    private boolean closeConnection = false;

    /**CPU**/
    private LineChart lineChartCPU;
    private LineData lineDataCPU;
    private LineDataSet lineDataSetCPU;
    private ArrayList lineEntriesCPU;
    private XAxis xAxisCPU;
    private YAxis yAxisLCPU;
    private YAxis yAxisRCPU;
    /**Memory**/
    private LineChart lineChartMemory;
    private LineData lineDataMemory;
    private LineDataSet lineDataSetMemory;
    private ArrayList lineEntriesMemory;
    private XAxis xAxisMemory;
    private YAxis yAxisLMemory;
    private YAxis yAxisRMemory;
    /**Memory**/
    private LineChart lineChartCpuTemperature;
    private LineData lineDataCpuTemperature;
    private LineDataSet lineDataSetCpuTemperature;
    private ArrayList lineEntriesCpuTemperature;
    private XAxis xAxisCpuTemperature;
    private YAxis yAxisLCpuTemperature;
    private YAxis yAxisRCpuTemperature;

    private float x = 0f;
    private float y = 0f;

    protected void lineChartCpuLoadPreparation(){
        lineChartCPU = findViewById(R.id.lineChartCpu);
//        lineChartCPU.getLegend().setEnabled(false);
        lineChartCPU.getDescription().setEnabled(false);
        lineChartCPU.setBackgroundColor(Color.TRANSPARENT);
        lineChartCPU.setDrawGridBackground(false);

        xAxisCPU = lineChartCPU.getXAxis();
        xAxisCPU.setAxisMinimum(0f);
        xAxisCPU.setGranularity(1f);
        xAxisCPU.setAxisMaximum(10f);
        xAxisCPU.setDrawLabels(false);
        xAxisCPU.setDrawGridLines(false);
        xAxisCPU.setDrawAxisLine(false);
        //xAxisCPU.setPosition(xAxisCPU.getPosition());

        yAxisLCPU = lineChartCPU.getAxisLeft();
        yAxisLCPU.setAxisMinimum(0f);
        yAxisLCPU.setGranularity(10f);
        yAxisLCPU.setAxisMaximum(100.f);
        yAxisLCPU.setDrawGridLines(false);
        yAxisLCPU.setDrawAxisLine(false);

        yAxisRCPU = lineChartCPU.getAxisRight();
        yAxisRCPU.setAxisMinimum(0f);
        yAxisRCPU.setGranularity(10f);
        yAxisRCPU.setAxisMaximum(100.f);
        yAxisRCPU.setDrawGridLines(true);

        lineEntriesCPU = new ArrayList<>();
    }
    protected void lineChartCpuLoadStart(float x, float cpuLoad){
        /****** CPU LOAD ********/
        lineDataSetCPU = new LineDataSet(lineEntriesCPU, "Cpu load");
        lineDataSetCPU.setLabel("Cpu load");
        lineDataSetCPU.setColor(Color.DKGRAY);
        lineDataSetCPU.setCircleColor(Color.BLUE);
        lineDataSetCPU.setDrawCircleHole(false);
        lineDataSetCPU.setCircleRadius(5f);
//        lineDataSetCPU.setDrawCircles(false);
        lineDataCPU = new LineData(lineDataSetCPU);
        lineDataCPU.setDrawValues(false);
        lineChartCPU.setData(lineDataCPU);

        if(lineEntriesCPU.size() >= 21){
            lineEntriesCPU.remove(0);
            float xMin = xAxisCPU.getAxisMinimum();
            float xMax = xAxisCPU.getAxisMaximum();
            xAxisCPU.setAxisMinimum((float) (xMin + 0.5));
            xAxisCPU.setAxisMaximum((float) (xMax + 0.5));
        }

        lineEntriesCPU.add(new Entry(x, cpuLoad));
        lineChartCPU.invalidate();
    }
    protected void lineChartCpuLoadFinish(){
        lineEntriesCPU.clear();
        lineEntriesCPU.clear();
        lineDataSetCPU.clear();
        lineChartCPU.clear();
    }

    protected void lineChartMemoryLoadPreparation(){
        lineChartMemory = findViewById(R.id.lineChartMemory);
//        lineChartMemory.getLegend().setEnabled(false);
        lineChartMemory.getDescription().setEnabled(false);
        lineChartMemory.setBackgroundColor(Color.TRANSPARENT);
        lineChartMemory.setDrawGridBackground(false);

        xAxisMemory = lineChartMemory.getXAxis();
        xAxisMemory.setAxisMinimum(0f);
        xAxisMemory.setGranularity(1f);
        xAxisMemory.setAxisMaximum(10f);
        xAxisMemory.setDrawLabels(false);
        xAxisMemory.setDrawGridLines(false);
        xAxisMemory.setDrawAxisLine(false);

        yAxisLMemory = lineChartMemory.getAxisLeft();
        yAxisLMemory.setAxisMinimum(0f);
        yAxisLMemory.setGranularity(10f);
        yAxisLMemory.setAxisMaximum(100.f);
        yAxisLMemory.setDrawGridLines(false);
        yAxisLMemory.setDrawAxisLine(false);

        yAxisRMemory = lineChartMemory.getAxisRight();
        yAxisRMemory.setAxisMinimum(0f);
        yAxisRMemory.setGranularity(10f);
        yAxisRMemory.setAxisMaximum(100.f);
        yAxisRMemory.setDrawGridLines(true);

        lineEntriesMemory = new ArrayList<>();
    }
    protected void lineChartMemoryLoadStart(float x, float memoryLoad){
        /****** Memory LOAD ********/
        lineDataSetMemory = new LineDataSet(lineEntriesMemory, "Memory load percentage");
//        lineDataSetMemory.setDrawValues(true);
        lineDataSetMemory.setColor(Color.BLUE);
        lineDataSetMemory.setCircleColor(Color.YELLOW);
        lineDataSetMemory.setDrawCircleHole(false);
        lineDataSetMemory.setCircleRadius(5f);
        lineDataMemory = new LineData(lineDataSetMemory);
        lineDataMemory.setDrawValues(false);
        lineChartMemory.setData(lineDataMemory);

        if(lineEntriesMemory.size() >= 21){
            lineEntriesMemory.remove(0);
            float xMin = xAxisMemory.getAxisMinimum();
            float xMax = xAxisMemory.getAxisMaximum();
            xAxisMemory.setAxisMinimum((float) (xMin + 0.5));
            xAxisMemory.setAxisMaximum((float) (xMax + 0.5));
        }

        lineEntriesMemory.add(new Entry(x, memoryLoad));
        lineChartMemory.invalidate();
    }
    protected void lineChartMemoryLoadFinish(){
        lineEntriesMemory.clear();
        lineEntriesMemory.clear();
        lineDataSetMemory.clear();
        lineChartMemory.clear();
    }

    protected void lineChartCpuTemperatureLoadPreparation(){
        lineChartCpuTemperature = findViewById(R.id.lineChartCpuTemperature);
        lineChartCpuTemperature.getDescription().setEnabled(false);
        lineChartCpuTemperature.setBackgroundColor(Color.TRANSPARENT);
        lineChartCpuTemperature.setDrawGridBackground(false);

        xAxisCpuTemperature = lineChartCpuTemperature.getXAxis();
        xAxisCpuTemperature.setAxisMinimum(0f);
        xAxisCpuTemperature.setGranularity(1f);
        xAxisCpuTemperature.setAxisMaximum(10f);
        xAxisCpuTemperature.setDrawLabels(false);
        xAxisCpuTemperature.setDrawGridLines(false);
        xAxisCpuTemperature.setDrawAxisLine(false);

        yAxisLCpuTemperature = lineChartCpuTemperature.getAxisLeft();
        yAxisLCpuTemperature.setAxisMinimum(0f);
        yAxisLCpuTemperature.setGranularity(10f);
        yAxisLCpuTemperature.setAxisMaximum(100.f);
        yAxisLCpuTemperature.setDrawGridLines(false);
        yAxisLCpuTemperature.setDrawAxisLine(false);

        yAxisRCpuTemperature = lineChartCpuTemperature.getAxisRight();
        yAxisRCpuTemperature.setAxisMinimum(0f);
        yAxisRCpuTemperature.setGranularity(10f);
        yAxisRCpuTemperature.setAxisMaximum(100.f);
        yAxisRCpuTemperature.setDrawGridLines(true);

        lineEntriesCpuTemperature = new ArrayList<>();
    }
    protected void lineChartCpuTemperatureLoadStart(float x, float CpuTemperatureLoad){
        /****** CpuTemperature LOAD ********/
        lineDataSetCpuTemperature = new LineDataSet(lineEntriesCpuTemperature, "Cpu Temperature");
        lineDataSetCpuTemperature.setColor(Color.CYAN);
        lineDataSetCpuTemperature.setCircleColor(Color.RED);
        lineDataSetCpuTemperature.setDrawCircleHole(false);
        lineDataSetCpuTemperature.setCircleRadius(5f);

        lineDataCpuTemperature = new LineData(lineDataSetCpuTemperature);
        lineDataCpuTemperature.setDrawValues(false);
        lineChartCpuTemperature.setData(lineDataCpuTemperature);

        if(lineEntriesCpuTemperature.size() >= 21){
            lineEntriesCpuTemperature.remove(0);
            float xMin = xAxisCpuTemperature.getAxisMinimum();
            float xMax = xAxisCpuTemperature.getAxisMaximum();
            xAxisCpuTemperature.setAxisMinimum((float) (xMin + 0.5));
            xAxisCpuTemperature.setAxisMaximum((float) (xMax + 0.5));
        }

        lineEntriesCpuTemperature.add(new Entry(x, CpuTemperatureLoad));
        lineChartCpuTemperature.invalidate();
    }
    protected void lineChartCpuTemperatureLoadFinish(){
        lineEntriesCpuTemperature.clear();
        lineEntriesCpuTemperature.clear();
        lineDataSetCpuTemperature.clear();
        lineChartCpuTemperature.clear();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState
    ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
//        lineChartCpuLoadPreparation();

        mBtnOpen  = (Button) findViewById(R.id.btn_open);
        mBtnClose = (Button) findViewById(R.id.btn_close);
        mHost  = (EditText) findViewById(R.id.IP);
        mPort  = (EditText) findViewById(R.id.PORT);
        mViewer = (TextView) findViewById(R.id.viewer);
        mViewer.setMovementMethod(new ScrollingMovementMethod());

        mBtnClose.setEnabled(false);

        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOpenClick();
            }
        });

        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCloseClick();
            }
        });
    }
    private void onOpenClick()
    {
        HOST = mHost.getText().toString();
        PORT = Integer.parseInt(mPort.getText().toString());
        // Создание подключения
        mConnect = new Connection(HOST, PORT);
        // Открытие сокета в отдельном потоке
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String str1 = mConnect.openConnection();
                    runOnUiThread(new Runnable() {
                                      @Override
                                      public void run() {
                                          if (mViewer != null) {
                                              mViewer.append(str1);
                                          }
                                          mBtnClose.setEnabled(true);
                                          lineChartCpuLoadPreparation();
                                          lineChartMemoryLoadPreparation();
                                          lineChartCpuTemperatureLoadPreparation();
                                      }
                                  });
                    Log.d(Connection.LOG_TAG, "Соединение установлено");
                    Log.d(Connection.LOG_TAG, "(mConnect != null) = " + (mConnect != null));

                    x = 0f;
                    // Постоянная отправка сообщений
                    while(!closeConnection){
                        try {
                            String text = text = "GET_DATA";
                            /* отправляем на сервер данные */
                            mConnect.sendData(text.getBytes());
                            /* получаем данные с сервера */
                            final String str = mConnect.receiveData();
                            JSONObject jObject = new JSONObject(str);
                            final String hostname = jObject.getString("hostname");
                            final String username = jObject.getString("username");
                            final String uptime = jObject.getString("uptime");
                            final String processor = jObject.getString("processor");
                            final String frequency = jObject.getString("frequency");
                            final String cpuLoad = jObject.getString("cpuLoad");
                            final String totalMemory = jObject.getString("totalMemory");
                            final String freeMemory = jObject.getString("freeMemory");
                            final String memoryPercent = jObject.getString("memoryPercent");
                            final String cpuTemp = jObject.getString("cpuTemp");
                            /* выводим данные на экран */
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(mViewer != null){
                                        mViewer.setText("");
                                        mViewer.append("\n");
                                        mViewer.append("\n");
                                        mViewer.append("\n");
                                        mViewer.append("\n");
                                        mViewer.append("Hostname = " + hostname + "\n");
//                                        mViewer.append("Username = " + username + "\n");
                                        mViewer.append("Uptime = " + uptime + "\n");
                                        mViewer.append("Processor = " + processor + "\n");
                                        mViewer.append("Frequency = " + frequency + "\n");
//                                        mViewer.append("Cpu load = " + cpuLoad + "%" + "\n");
//                                        mViewer.append("Total memory = " + totalMemory + "\n");
//                                        mViewer.append("Free memory = " + freeMemory + "\n");
//                                        mViewer.append("Memory percent = " + memoryPercent + "%" + "\n");
//                                        mViewer.append("Cpu temperature = " + cpuTemp + "%" + "\n");
                                        final Layout layout = mViewer.getLayout();
                                        if(layout != null){
                                            int scrollDelta = layout.getLineBottom(mViewer.getLineCount() - 1) - mViewer.getScrollY() - mViewer.getHeight();
                                            if(scrollDelta > 0)
                                                mViewer.scrollBy(0, scrollDelta);
                                        }

                                        /****** CPU LOAD ********/
                                        lineChartCpuLoadStart(x, Float.parseFloat(cpuLoad));
                                        /****** Memory LOAD ********/
                                        lineChartMemoryLoadStart(x, Float.parseFloat(memoryPercent));
                                        /****** Cpu temperature LOAD ********/
                                        lineChartCpuTemperatureLoadStart(x, Float.parseFloat(cpuTemp));
                                        x = (float) (x + 0.5);
                                    }
                                }
                            });
//                            Log.e(Connection.LOG_TAG, str);
                        } catch (Exception e) {
                            Log.e(Connection.LOG_TAG, e.getMessage());
                        }
                        sleep(100);
                    }

                    // Закрытие соединения
                    closeConnect();

                    // Разблокирование кнопок в UI потоке
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            mBtnSend.setEnabled(false);
                            mBtnClose.setEnabled(false);
                            lineChartCpuLoadFinish();
                            lineChartMemoryLoadFinish();
                            lineChartCpuTemperatureLoadFinish();
                        }
                    });


//                    lineEntriesCPU.clear();
//                    lineDataSetCPU.clear();
//                    lineChartCPU.clear();

                } catch (Exception e) {
                    Log.e(Connection.LOG_TAG, e.getMessage());
                    mConnect = null;
                }
            }
        }).start();
    }

    private void closeConnect(){
        try {
            mConnect.sendData("EXIT".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Закрытие соединения
        mConnect.closeConnection();
        Log.d(Connection.LOG_TAG, "Соединение закрыто");
        onCloseClick();
    }

    private void onCloseClick()
    {
        closeConnection = !closeConnection;
    }

}
