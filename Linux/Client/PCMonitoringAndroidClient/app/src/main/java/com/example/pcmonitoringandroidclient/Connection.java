package com.example.pcmonitoringandroidclient;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class Connection
{
    private  Socket  mSocket = null;
    private  String  mHost   = null;
    private  int     mPort   = 0;

    // Входной поток получения данных из сокета
    private InputStream inputStream = null;

    public static final String LOG_TAG = "SOCKET";

    public Connection() {}

    public Connection (final String host, final int port)
    {
        this.mHost = host;
        this.mPort = port;
    }

    // Метод открытия сокета
    public String openConnection() throws Exception
    {
        // Если сокет уже открыт, то он закрывается
        closeConnection();
        try {

            Log.d("new Socket mHost = ",  String.valueOf(mHost));
            Log.d("new Socket mPort = ", String.valueOf(mPort));
            // Создание сокета
            mSocket = new Socket(mHost, mPort);

            //TODO test input
            try {
                // Определение входного потока
                inputStream = mSocket.getInputStream();
                byte[] data = new byte[1024*4];
                while(true) {
                    try {
                        /*
                         * Получение информации :
                         *    count - количество полученных байт
                         */
                        int count;

                        count=inputStream.read(data,0,data.length);
                       // Log.d(Connection.LOG_TAG, String.valueOf(count));

                        if (count > 0) {
                            String msg=new String(data, 0, count);
                            Log.d(Connection.LOG_TAG, "Server Message = "  + String.valueOf(msg));
                            break;
                            // Вывод в консоль сообщения
                            //System.out.println(msg);
                        } else if (count == -1 ) {
                            // Если count=-1, то поток прерван
                            //System.out.println("socket is closed");

                            //clientSocket.close();
                            break;
                        }
                    } catch (IOException e) {
//                        System.err.println();
                        Log.d(Connection.LOG_TAG, e.getMessage());
                        //return e.getMessage();
                    }
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Ошибка чтения данных при открытии сокета"
                        + e.getMessage());
                return e.getMessage();
            }

        } catch (IOException e) {
//            throw new Exception("Невозможно создать сокет: "
//                    + e.getMessage());
            return e.getMessage();
        }
        return "ok";
    }

    public String receiveData(){
        try {
            // Определение входного потока
            inputStream = mSocket.getInputStream();
            byte[] data = new byte[2048];
            //while(true) {
                try {
                    /*
                     * Получение информации :
                     *    count - количество полученных байт
                     */
                    int count;

                    count=inputStream.read(data,0,data.length);
                    Log.d(Connection.LOG_TAG, "Server Message = "  +  String.valueOf(count));
                    // Log.d(Connection.LOG_TAG, String.valueOf(count));

                    if (count > 0) {
                        String msg = new String(data, 0, count);
                        Log.d(Connection.LOG_TAG, "Server Message = "  +  String.valueOf(msg));
                        return msg;
                        //break;
                        // Вывод в консоль сообщения
                        //System.out.println(msg);
                    } else if (count == -1 ) {
                        return "";
                        // Если count=-1, то поток прерван
                        //System.out.println("socket is closed");

                        //clientSocket.close();
                        //break;
                    }
                } catch (IOException e) {
//                        System.err.println();
                    Log.d(Connection.LOG_TAG, e.getMessage());
                }
            //}
        } catch (IOException e) {
            Log.e(LOG_TAG, "Ошибка чтения данных при открытии сокета"
                    + e.getMessage());
        }
        return "";
    }
    /**
     * Метод закрытия сокета
     */
    public void closeConnection()
    {
        if (mSocket != null && !mSocket.isClosed()) {
            try {
                mSocket.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Ошибка при закрытии сокета :"
                        + e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mSocket = null;
            }
        }
        mSocket = null;
    }
    /**
     * Метод отправки данных
     */
    public void sendData(byte[] data) throws Exception {
        // Проверка открытия сокета
        if (mSocket == null || mSocket.isClosed()) {
            throw new Exception("Ошибка отправки данных. " +
                    "Сокет не создан или закрыт");
        }
        // Отправка данных
        try {
            mSocket.getOutputStream().write(data);
            mSocket.getOutputStream().flush();
        } catch (IOException e) {
            throw new Exception("Ошибка отправки данных : "
                    + e.getMessage());
        }
    }
    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        closeConnection();
    }
}